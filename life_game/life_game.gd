extends Node2D


var matrix: Array
var rng := RandomNumberGenerator.new()
var dot := Rect2(0, 0, 1, 1)


func _ready():
	rng.randomize()
	var matrix_size := get_viewport().get_visible_rect().size
	
	for x in range(matrix_size.x):
		matrix.append([])
		for y in range(matrix_size.y):
			matrix[x].append(rng.randi_range(0, 1))


func _process(_delta):
	_iterate()
	queue_redraw()


func _draw():
	for x in range(matrix.size()):
		for y in range(matrix[x].size()):
			if matrix[x][y]:
				dot.position.x = x
				dot.position.y = y
				draw_rect(dot, Color.DIM_GRAY)


func _iterate():
	var new_matrix := []
	
	for x in range(matrix.size()):
		new_matrix.append([])
		for y in range(matrix[x].size()):
			var alive_neighborhood =  _get_alive_neighborhood(x, y)
			var new_cell : bool = matrix[x][y]
			
			if matrix[x][y] and (alive_neighborhood < 2 or alive_neighborhood > 3):
				new_cell = false
			elif !matrix[x][y] and alive_neighborhood == 3:
				new_cell = true
			
			new_matrix[x].append(new_cell)
	
	matrix = new_matrix


func _get_alive_neighborhood(x, y) -> int:
	var alive_count := 0
	var matrix_width := matrix.size()
	var matrix_height : int = matrix[x].size()
	
	for dx in [-1, 0, 1]:
		for dy in [-1, 0, 1]:
			if dx == 0 and dy == 0:
				continue
			
			var nx = (x + dx) % matrix_width
			if nx < 0:
				nx += matrix_width
			
			var ny = (y + dy) % matrix_height
			if ny < 0:
				ny += matrix_height
			
			if matrix[nx][ny]:
				alive_count += 1

	return alive_count
