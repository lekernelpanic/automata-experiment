extends Node2D


var matrix: Array
var rng := RandomNumberGenerator.new()
var dot := Rect2(0, 0, 1, 1)


func _ready():
	rng.randomize()
	var matrix_size := get_viewport().get_visible_rect().size
	
	for x in range(matrix_size.x):
		matrix.append([])
		for y in range(matrix_size.y):
			matrix[x].append(Cell.new(rng.randi_range(0, 1)))
	
	for x in range(matrix.size()):
		for y in range(matrix[x].size()):
			matrix[x][y].fill_near_cells(matrix, x, y)


func _process(_delta):
	_iterate()
	queue_redraw()


func _draw():
	for x in range(matrix.size()):
		for y in range(matrix[x].size()):
			if matrix[x][y].state:
				dot.position.x = x
				dot.position.y = y
				draw_rect(dot, Color.DIM_GRAY)


func _iterate():
	for x in range(matrix.size()):
		for y in range(matrix[x].size()):
			
			var cell = matrix[x][y]
			var near_cell: int = cell.get_alive_near_cells()
			
			if cell.state and (near_cell < 2 or near_cell > 3):
				cell.future_state = false
			elif !cell.state and near_cell == 3:
				cell.future_state = true
	
	for line: Array in matrix:
		for cell: Cell in line:
			cell.state = cell.future_state
