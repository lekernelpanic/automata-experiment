class_name Threadpool


var threads : Array[Thread]
var tasks: Array[Callable]


func _init():
	for i in range(OS.get_processor_count()):
		var thread := Thread.new()
		threads.append(thread)


func append_task(task: Callable):
	tasks.append(task)


func run():
	while not tasks.is_empty():
		for thread in threads:
			if not thread.is_alive() and not tasks.is_empty():
				if thread.is_started():
					thread.wait_to_finish()
				thread.start(tasks[tasks.size() - 1])
				tasks.remove_at(tasks.size() - 1)
	
	for thread in threads:
		thread.wait_to_finish()
