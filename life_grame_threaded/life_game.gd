extends Node2D


var matrix: Array
var rng := RandomNumberGenerator.new()
var dot := Rect2(0, 0, 1, 1)
var threadpool: Threadpool


func _ready():
	threadpool = Threadpool.new()
	
	rng.randomize()
	var matrix_size := get_viewport().get_visible_rect().size
	
	for x in range(matrix_size.x):
		matrix.append([])
		for y in range(matrix_size.y):
			matrix[x].append(rng.randi_range(0, 1))


func _process(_delta):
	_iterate()
	queue_redraw()


func _draw():
	for x in range(matrix.size()):
		for y in range(matrix[x].size()):
			if matrix[x][y]:
				dot.position.x = x
				dot.position.y = y
				draw_rect(dot, Color.DIM_GRAY)


func _iterate():
	var new_matrix: Array
	for x in range(matrix.size()):
		new_matrix.append([])
		for y in range(matrix[x].size()):
			new_matrix[x].append(matrix[x][y])
			threadpool.append_task(_manage_cell.bind(x, y, new_matrix))
	
	threadpool.run()
	matrix = new_matrix


func _manage_cell(x: int, y: int, new_matrix: Array):
	var alive_neighborhood =  _get_alive_neighborhood(x, y)
	var new_cell : int = matrix[x][y]
	
	if matrix[x][y] and (alive_neighborhood < 2 or alive_neighborhood > 3):
		new_cell = 0
	elif !matrix[x][y] and alive_neighborhood == 3:
		new_cell = 1
	
	new_matrix[x][y] = new_cell


func _get_alive_neighborhood(x: int, y: int) -> int:
	var alive_count := 0
	var matrix_width := matrix.size()
	var matrix_height: int = matrix[x].size()

	for dx in [-1, 0, 1]:
		for dy in [-1, 0, 1]:
			if dx == 0 and dy == 0:
				continue

			var nx = x + dx
			var ny = y + dy

			if nx >= 0 and nx < matrix_width and ny >= 0 and ny < matrix_height:
				if matrix[nx][ny]:
					alive_count += 1

	return alive_count

