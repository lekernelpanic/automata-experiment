class_name Cell


var state: int
var future_state: int
var near_cells : Array[Cell]


func _init(new_state: bool):
	state = new_state


func fill_near_cells(matrix: Array, x: int, y: int):
	var matrix_width := matrix.size()
	var matrix_height : int = matrix[x].size()
	
	for dx in [-1, 0, 1]:
		for dy in [-1, 0, 1]:
			if dx == 0 and dy == 0:
				continue
			
			var nx = (x + dx) % matrix_width
			if nx < 0:
				nx += matrix_width
			
			var ny = (y + dy) % matrix_height
			if ny < 0:
				ny += matrix_height
			
			near_cells.append(matrix[nx][ny])

func get_alive_near_cells() -> int:
	var count := 0
	for cell: Cell in near_cells:
		if cell.state:
			count += 1
	return count
