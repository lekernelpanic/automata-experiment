extends Node2D


const RATIO := 0.125


var matrix: Array
var rng := RandomNumberGenerator.new()
var dot := Rect2(0, 0, 1, 1)
var rules: Array


func _ready():
	rng.randomize()
	dot = Rect2(0, 0, 1 / RATIO, 1 / RATIO)
	
	_new_matrix()
	_new_rules()


func _process(_delta):
	_iterate()
	queue_redraw()
	
	if Input.is_action_just_pressed("ui_accept"):
		_new_matrix()
		_new_rules()


func _draw():
	for x in range(matrix.size()):
		for y in range(matrix[x].size()):
			if matrix[x][y].state:
				dot.position.x = x / RATIO
				dot.position.y = y / RATIO
				draw_rect(dot, Color.DIM_GRAY)


func _iterate():
	for x in range(matrix.size()):
		for y in range(matrix[x].size()):
			
			var cell = matrix[x][y]
			var near_cell: int = cell.get_alive_near_cells()
				
			if rules[cell.state][near_cell] == 0:
				cell.future_state = false
			elif rules[cell.state][near_cell] == 1:
				cell.future_state = cell.state
			else:
				cell.future_state = true
	
	for line: Array in matrix:
		for cell: Cell in line:
			cell.state = cell.future_state


func _new_matrix():
	matrix.clear()
	
	var matrix_size := get_viewport().get_visible_rect().size * RATIO
	print(matrix_size)
	
	for x in range(matrix_size.x):
		matrix.append([])
		for y in range(matrix_size.y):
			matrix[x].append(Cell.new(rng.randi_range(0, 1)))
	
	for x in range(matrix.size()):
		for y in range(matrix[x].size()):
			matrix[x][y].fill_near_cells(matrix, x, y)


func _new_rules():
	rules.clear()
	for i in range(2):
		rules.append([])
		for j in range(9):
			rules[i].append(rng.randi_range(0, 2))
	$rules.text = str(rules)
