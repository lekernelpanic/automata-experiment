extends Node2D


var matrix: Array[bool]
var matrix_width: int
var rng := RandomNumberGenerator.new()
var dot := Rect2(0, 0, 1, 1)


func _ready():
	rng.randomize()
	var viewport_size := get_viewport().get_visible_rect().size
	matrix_width = int(viewport_size.x)
	_populate(int(viewport_size.x * viewport_size.y))


func _process(_delta):
	for i in range(matrix.size()):
		_get_block(i)
	queue_redraw()


func _draw():
	for i in range(matrix.size()):
		if matrix[i]:
			dot.position.x = i % matrix_width
			dot.position.y = int(i / float(matrix_width))
			draw_rect(dot, Color.DIM_GRAY)


func _populate(size: int):
	for i in range(size):
		if rng.randi_range(0, 1):
			matrix.append(true)
		else:
			matrix.append(false)


func _get_block(cell_index):
	var indexes := [
		matrix_width - cell_index - 1,
		matrix_width - cell_index,
		matrix_width - cell_index + 1,
		cell_index - 1,
		cell_index,
		cell_index + 1,
		matrix_width + cell_index - 1,
		matrix_width + cell_index,
		matrix_width + cell_index + 1,
	]
	var block: Array[bool] = []
	for index in indexes:
		if index >= 0 and index < matrix.size():
			block.append(matrix[index])
	
	return block
